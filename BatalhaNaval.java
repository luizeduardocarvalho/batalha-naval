/* ==============================================================
 * Batalha Naval - versão simples para Fundamentos de Programação
 * Daniel Callegari 2017
 * ============================================================*/
 
import java.util.*;
import java.io.*;

public class BatalhaNaval {
    private static final int MAXTAB = 8;            // Tamanho do tabuleiro NxN
    private static final int MAXVIDAS = 15;         // Vidas do jogador
    private static int vidasRestantes = MAXVIDAS;   // Vidas restantes
    private static int totalPartes = 0;             // Partes de embarcações
    private static int totalPartesAcertadas = 0;    // Quantas partes afundadas
    private static final char[][] tabuleiroEscondido = new char[MAXTAB][MAXTAB];
    private static final char[][] tabuleiroJogador = new char[MAXTAB][MAXTAB];
    private static Scanner teclado = new Scanner(System.in);
    private static String mensagem = "Começou a batalha!";
    
    public static void main(String[] args) {
        inicializa();
        
        while (vidasRestantes > 0 && totalPartesAcertadas < totalPartes) {
            exibeTabuleiro();
            processaJogada();
        }
        
        finaliza();        
    }

    private static void inicializa() {
        // Cria strings temporárias para facilitar a definição do tabuleiro
        // ~ - água
        // 1 - lancha       (apenas 1 parte)
        // 2 - barco        (contém 2 partes)
        // 3 - navio        (contém 3 partes)
        // 4 - porta-aviões (contém 4 partes)

        InicializaTabela(tabuleiroEscondido);

        // gera um numero aleatorio de barcos de 3 a 5
        int numeroBarcos = GeraNumeroAleatorio(6);
        switch(numeroBarcos) {
            case 0:
                numeroBarcos+=3;
                break;
                
            case 1:
                numeroBarcos+=2;
                break;
                
            case 2:
                numeroBarcos+=1;
                break;
        }

        PopularTabela(tabuleiroEscondido, numeroBarcos);

        // cria strings para popular o tabuleiro escondido
        String t0 = new String(tabuleiroEscondido[0]);
        String t1 = new String(tabuleiroEscondido[1]);
        String t2 = new String(tabuleiroEscondido[2]);
        String t3 = new String(tabuleiroEscondido[3]);
        String t4 = new String(tabuleiroEscondido[4]);
        String t5 = new String(tabuleiroEscondido[5]);
        String t6 = new String(tabuleiroEscondido[6]);
        String t7 = new String(tabuleiroEscondido[7]);
        
        String[] tempTab = new String[MAXTAB];
        //           |01234567| Monte o tabuleiro escondido!
        tempTab[0] = t0;
        tempTab[1] = t1;
        tempTab[2] = t2;
        tempTab[3] = t3;
        tempTab[4] = t4;
        tempTab[5] = t5;
        tempTab[6] = t6;
        tempTab[7] = t7;

        // Guarda o tabuleiro na matriz, a partir das Strings
        for (int linha = 0; linha < MAXTAB; linha++) {
            for (int coluna = 0; coluna < MAXTAB; coluna++) {
                char parte =  tempTab[linha].charAt(coluna);
                
                if (parte != '~') {   // Se não for água,
                    totalPartes++;    // conta a parte
                }
                
                tabuleiroEscondido[linha][coluna] = parte;
                tabuleiroJogador[linha][coluna] = '.';
            }
        }
        
    }

    private static void exibeTabuleiro() {
        // System.out.print("\f"); // Limpa a tela
        ClearScreen(); // limpa a tela do terminal
        System.out.println();
        System.out.println("===================");
        System.out.println("   BATALHA NAVAL   ");
        System.out.println("===================");
        
        // Mostra números das colunas
        System.out.print("  ");
        for (int c = 0; c < MAXTAB; c++) {
            System.out.print(" " + (c+1));
        }
        System.out.println();
        
        // Mostra cada linha do tabuleiro do jogador, com um número na frente
        for (int l = 0; l < MAXTAB; l++) {
            System.out.print(" " + (l+1));
            for (int c = 0; c < MAXTAB; c++) {
                System.out.print(" " + tabuleiroJogador[l][c]);
            }
            System.out.println();
        }
        
        // Mostra mensagens
        System.out.println();  
        System.out.println(mensagem);
        System.out.println();
        System.out.printf("Pontos: %d de %d\n", totalPartesAcertadas, totalPartes);
        System.out.println("Vidas restantes: " + vidasRestantes);
    }

    private static void processaJogada() {
        System.out.println("Faça sua jogada!");
        
        int linha  = obtemValor("  Linha : ");
        int coluna = obtemValor("  Coluna: ");
        
        // Copia o que estiver no tabuleiro escondido
        // para o tabuleiro do jogador
        tabuleiroJogador[linha-1][coluna-1] = tabuleiroEscondido[linha-1][coluna-1];
        
        // Verifica o que há na posição
        switch (tabuleiroEscondido[linha-1][coluna-1]) {
            case '~':
                mensagem = "~~~ ÁGUA!";
                vidasRestantes--;
                break;

            case '1':
                mensagem = "BOOM! Acertou uma lancha!";
                totalPartesAcertadas++;
                break;

            case '2':
                mensagem = "BOOM! Acertou parte de um barco!";
                totalPartesAcertadas++;
                break;

            case '3':
                mensagem = "BOOM! Acertou parte de um navio!";
                totalPartesAcertadas++;
                break;

            case '4':
                mensagem = "BOOM! Acertou parte de um porta-aviões!";
                totalPartesAcertadas++;
                break;

            default:
                mensagem = "* BUG NO PROGRAMA *";
                vidasRestantes = 0;
        }
    }

    private static void finaliza() {
        exibeTabuleiro();
        System.out.println("Fim da batalha!");
        
        if (vidasRestantes > 0) {
            System.out.println("Você venceu!");
        } else {
            System.out.println("Você perdeu...");
        }
        
        System.out.println("===================");
    }

    private static int obtemValor(String strValor) {
        int valorUsuario = -1;
        
        // Garante que o usuário entra com um valor
        // válido entre 1 e MAXTAB.
        do {
            System.out.print(strValor);
            valorUsuario = teclado.nextInt();

            if (valorUsuario < 1 || valorUsuario > MAXTAB) {
                System.out.println("  Valor inválido!");
            }
        } while (valorUsuario < 1 || valorUsuario > MAXTAB);
        
        return valorUsuario;
    }

    public static void InicializaTabela(char[][] tabuleiroEscondido)
    {
        for(int lin = 0; lin < MAXTAB; lin++) {
            for(int col = 0; col < MAXTAB; col++) {
                tabuleiroEscondido[lin][col] = '~';
            }
        }
    }

    public static int GeradorDeBarcos()
    {        
        int tamanhoBarco = GeraNumeroAleatorio(6);
        if(tamanhoBarco == 0) {
            tamanhoBarco++; // barco nao pode ter tamanho 0
        }

        return tamanhoBarco;
    }

    public static void PopularTabela(char[][] tabuleiroEscondido, int numeroBarcos)
    {
        while(numeroBarcos > 0) {
            int tamanhoBarco = GeradorDeBarcos(); // gera o tamanho do barco
            int[] posicaoInicial = PosicaoInicialBarco(); // gera um lugar aleatorio para o barco ser posicionado
            Boolean espacoLivre = ChecaSeEspacoEstaLivre(tabuleiroEscondido, posicaoInicial[0], posicaoInicial[1]);       
        
            if(espacoLivre) {

                char direcaoBarco = GeraDirecaoBarco();
                
                Boolean podeColocarBarco = ChecaNEspacos(tabuleiroEscondido, posicaoInicial[0], posicaoInicial[1], direcaoBarco, tamanhoBarco);

                if(podeColocarBarco) {
    
                    PosicionaBarco(tabuleiroEscondido, posicaoInicial[0], posicaoInicial[1], direcaoBarco, tamanhoBarco);
                    numeroBarcos--;
                }
            }
        }
    }

    public static Boolean ChecaSeEspacoEstaLivre(char[][] tabuleiroEscondido, int lin, int col)
    {
        Boolean estaLivre;

        if(tabuleiroEscondido[lin][col] == '~') {
            estaLivre = true;
        } else {
            estaLivre = false;
        }

        return estaLivre;
    }

    public static int[] PosicaoInicialBarco()
    {

        int lin = GeraNumeroAleatorio(MAXTAB);
        int col = GeraNumeroAleatorio(MAXTAB);

        int[] posicaoInicial = new int[2];

        posicaoInicial[0] = lin;
        posicaoInicial[1] = col;

        return posicaoInicial;
    }

    public static void PosicionaBarco(char[][] tabuleiroEscondido, int lin, int col, char direcao, int tamanhoBarco)
    {
        switch(direcao) {
            // caso o barco seja montado para cima
            case 't':
                for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                    if(lin-tamanhoBarco >= 0) {
                        tabuleiroEscondido[lin-espacos][col] = (char)(tamanhoBarco + 48);
                    }
                }
                break;

            // caso o barco seja montado para baixo
            case 'b':
                if(lin+tamanhoBarco <= MAXTAB) {
                    for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                        tabuleiroEscondido[lin+espacos][col] = (char)(tamanhoBarco + 48);
                    }
                }
                break;

            // caso o barco seja montado para a esquerda
            case 'l':
                if(col-tamanhoBarco >= 0) {
                    for(int espacos = 0; espacos < tamanhoBarco; espacos++) {                
                        tabuleiroEscondido[lin][col-espacos] = (char)(tamanhoBarco + 48);
                    }
                }
                break;
            
            // caso o barco seja montado para a direita
            case 'r':
                if(col+tamanhoBarco <= MAXTAB) {
                    for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                        tabuleiroEscondido[lin][col+espacos] = (char)(tamanhoBarco + 48);
                    }
                }
                break;
        }
    }

    // checa se existem espacos suficientes para montar um barco de n espacos comecando na lin e col
    public static Boolean ChecaNEspacos(char[][] tabuleiroEscondido, int lin, int col, char direcao, int tamanhoBarco)
    {
        switch(direcao) {
            // para cima
            case 't':
                for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                    if(lin-tamanhoBarco < 0 || tabuleiroEscondido[lin-espacos][col] != '~') { // nao pode sair para fora do tabuleiro
                        return false;
                    }
                }
                break;
            
            // para baixo
            case 'b':
                for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                    if(lin+tamanhoBarco > MAXTAB-1 || tabuleiroEscondido[lin+espacos][col] != '~') { // nao pode sair para fora do tabuleiro
                        return false;
                    }
                }
                break;

            // para a esquerda
            case 'l':
                for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                    if(col-tamanhoBarco < 0 || tabuleiroEscondido[lin][col-espacos] != '~') { // nao pode sair para fora do tabuleiro
                        return false;
                    }
                }
                break;

            // para a direita
            case 'r':
                for(int espacos = 0; espacos < tamanhoBarco; espacos++) {
                    if(col+tamanhoBarco > MAXTAB-1 || tabuleiroEscondido[lin][col+espacos] != '~') { // nao pode sair para fora do tabuleiro
                        return false;
                    }
                }
                break;
        }

        return true;
    }

    public static char GeraDirecaoBarco()
    {
        int randomDir = GeraNumeroAleatorio(4);
        char direcao = '-';

        switch(randomDir) {
            case 0:
                direcao = 't';
                break;
            
            case 1:
                direcao = 'b';
                break;
            
            case 2:
                direcao = 'l';
                break;
            
            case 3:
                direcao = 'r';
                break;
        }

        return direcao;
    }

    public static int GeraNumeroAleatorio(int intervaloNum)
    {
        Random randomNumber = new Random();

        int randNum = randomNumber.nextInt(intervaloNum);

        return randNum;
    }

    public static void ClearScreen()
    {
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }
    
}
